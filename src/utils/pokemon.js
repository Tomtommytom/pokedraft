import { convertRgbToRgbAlpha } from './colorStringOperations'
import pokedex from '@/assets/pokedex.json'
import Item from './itemCategory'
import { calculateWeaknesses, getKeys } from './weaknessInfo'

const keys = getKeys()

export default class Pokemon {
  constructor(
    {
      name,
      id,
      baseStats,
      abilities,
      trivia,
      sprite,
      types,
      evos,
      prevos,
      nature
    },
    initalPokemon,
    heldItem
  ) {
    this.name = name
    this.id = id
    this.baseStats = baseStats
    this.abilities = abilities
    this.sprite = sprite
    this.types = types
    this.weakness = calculateWeaknesses(...this.types)
    this.trivia = trivia
    this.heldItem = heldItem || new Item()
    this.selectedAbility = abilities['0']
    this.nature = nature || randomNature()
    this.evos = evos
    this.prevos = prevos
    this.price = this.getSumOfStats()
    this.evs = [0, 0, 0, 0, 0, 0]
    if ((evos || prevos) && initalPokemon) {
      this.setEvolutionLine()
    }
  }

  setEvolutionLine() {
    if (this.evos) {
      this.evos = this.evos.map(
        evo => new Pokemon(pokedex[evo]),
        false,
        this.heldItem
      )
    }
    if (this.prevos) {
      this.prevos = this.prevos.map(
        prevo => new Pokemon(pokedex[prevo]),
        false,
        this.heldItem
      )
    }
    this.evolutions = []
    this.evolutions.push(
      new Pokemon(pokedex[this.name]),
      this.prevos,
      this.evos
    )
    this.evolutions = this.evolutions.flat(1).filter(evo => !!evo)
  }

  getStatsObject() {
    //returns {hp: number, attack: number, special-attack: number...}
    return this.baseStats
  }

  getAbilityArray() {
    return Object.values(this.abilities)
  }

  getAbilityString() {
    return this.getAbilityArray().join('/')
  }

  getSumOfStats() {
    return Object.values(this.baseStats).reduce((acc, curr) => {
      return acc + curr
    }, 0)
  }

  getSprite() {
    return this.sprite
  }

  getMainType() {
    return this.types[0]
  }

  getSecondaryType() {
    return this.types[1]
  }

  getColorForType(alpha) {
    return convertRgbToRgbAlpha(
      TYPE_STRING_COLOR[this.getMainType().toLowerCase()],
      alpha
    )
  }

  getStatOrderedArray() {
    return statOrder.map(stat => this.getStatsObject()[stat])
  }

  getMatchupToType(type) {
    return this.getTypeMatchups().filter(
      typeObj => typeObj.typeName === type
    )[0].typeValue
  }

  getTypeMatchups() {
    return keys.map(key => {
      return { typeName: key, typeValue: this.weakness[key] }
    })
  }

  getWeaknesses() {
    return keys
      .filter(key => this.weakness[key] > 1)
      .map(key => {
        return {
          typeName: key,
          typeValue: this.weakness[key]
        }
      })
  }

  getResistances() {
    return keys
      .filter(key => this.weakness[key] < 1 && this.weakness[key] !== 0)
      .map(key => {
        return {
          typeName: key,
          typeValue: this.weakness[key]
        }
      })
  }

  getImmunities() {
    return keys
      .filter(key => this.weakness[key] === 0)
      .map(key => {
        return {
          typeName: key,
          typeValue: this.weakness[key]
        }
      })
  }

  getStatLabels() {
    return statOrder.map(stat => statStrings[stat])
  }

  getNameLabel() {
    return this.name.toUpperCase()
  }

  getTier() {
    const sum = this.getSumOfStats()

    if (sum > 600) return 'S'
    else if (sum >= 400) return 'A'
    else return 'B'
  }

  getReward() {
    return 2 * (720 - this.getSumOfStats())
  }

  getPrice() {
    return this.price
  }

  getEvolvePrice() {
    return this.prevos ? this.prevos.length * 200 : 0
  }

  setPrice(newValue) {
    console.log('you have the price to')
    this.price = newValue
    console.log(this.price)
  }

  getHeldItemName() {
    return this.heldItem.name
      .split('-')
      .map(name => capFirstChar(name))
      .join(' ')
  }

  getHeldItemSmogonString() {
    if (this.heldItem.name === 'Coin Amulet') {
      return ''
    } else {
      return this.getHeldItemName()
    }
  }

  toSmogonString() {
    return [
      `${capFirstChar(this.name)} @ ${this.getHeldItemSmogonString()}`,
      `Ability: ${capFirstChar(this.selectedAbility, '-')}`,
      `EVs: ${this.getEvString()}`,
      `${capFirstChar(this.nature)} Nature`
    ]
  }

  getEvString() {
    return this.evs
      .map((ev, index) =>
        ev ? `${ev} ${smogonStatStrings[statOrder[index]]}` : ''
      )
      .filter(ev => !!ev)
      .join(' / ')
  }

  addEvolutions(arrayOfPokemon) {
    if (arrayOfPokemon) {
      this.evolutions = arrayOfPokemon.map(evo => new Pokemon(evo))
    }
  }

  getEvolutions() {
    return this.evolutions
      ? this.evolutions.filter(evo => evo.name !== this.name)
      : undefined
  }

  hasEvolutions() {
    return !!this.evolutions
  }

  logEvolutionNamesWithIndex() {
    this.evolutions.map((evo, index) => `name: ${evo.name}, index:${index}`)
  }

  evolveTo(pokemon) {
    if (this.evolutions.includes(pokemon) && this.hasEvolutions()) {
      const carry = this.getCarryValues()
      Object.assign(this, pokemon, carry)
    }
  }

  getCarryValues() {
    return {
      evs: this.evs,
      nature: this.nature,
      heldItem: this.heldItem,
      evolutions: this.evolutions
    }
  }
}

const TYPE_STRING_COLOR = {
  normal: 'rgb(170,170,153)',
  fire: 'rgb(255,68,34)',
  water: 'rgb(51,153,255)',
  electric: 'rgb(255,204,51)',
  grass: 'rgb(119,204,85)',
  ice: 'rgb(102,204,255)',
  fighting: 'rgb(187,85,68)',
  poison: 'rgb(170,85,153)',
  ground: 'rgb(221,187,85)',
  flying: 'rgb(136,153,255)',
  psychic: 'rgb(255,85,153)',
  bug: 'rgb(170,187,34)',
  rock: 'rgb(187,170,102)',
  ghost: 'rgb(102,102,187)',
  dragon: 'rgb(119,102,238)',
  dark: 'rgb(119,85,68)',
  steel: 'rgb(170,170,187)',
  fairy: 'rgb(238,153,238)'
}

const statStrings = {
  hp: 'HP',
  atk: 'ATK',
  def: 'DEF',
  spa: 'SPATK',
  spd: 'SPDEF',
  spe: 'SPEED'
}

const smogonStatStrings = {
  hp: 'HP',
  atk: 'Atk',
  def: 'Def',
  spa: 'SpA',
  spd: 'SpD',
  spe: 'Spe'
}

const statOrder = ['hp', 'atk', 'def', 'spe', 'spd', 'spa']

const natures = [
  'hardy',
  'lonely',
  'brave',
  'adamant',
  'naughty',
  'bold',
  'docile',
  'relaxed',
  'impish',
  'lax',
  'timid',
  'hasty',
  'serious',
  'jolly',
  'naive',
  'modest',
  'mild',
  'quiet',
  'bashful',
  'rash',
  'calm',
  'gentle',
  'sassy',
  'careful',
  'quirky'
]

const randomNature = () => {
  const randIndex = Math.floor(Math.random() * natures.length)
  return natures[randIndex]
}

const capFirstChar = (string, split = ' ') => {
  const strings = string.split(split)
  return strings
    .map(string => string.charAt(0).toUpperCase() + string.slice(1))
    .join(split)
}
