// import pokeApiService from '@/apis/pokeApiService'
import Pokemon from './pokemon'
import pokedex from '@/assets/pokedex.json'
import keys from '@/assets/keys.json'

const draftSixPokemon = () => {
  let ids = generateSixRandomids()
  return ids.map(id => new Pokemon(pokedex[keys[id]], true))
}

const draftSixMeowth = () => {
  return meowthArray.map(id => {
    const meowth = new Pokemon(
      { ...pokedex[keys[id]], nature: 'naive' },
      true,
      {
        name: 'Coin Amulet',
        id: 'coin-amulet',
        desc: "You should've spent your money better."
      }
    )
    meowth.setPrice(0)
    return meowth
  })
}

const generateSixRandomids = () => {
  const result = new Array(6).fill(false)
  return result.map(() => Math.floor(Math.random() * 890 + 1))
}

const generateRandomItem = () => {
  return 'hi'
}

const meowthArray = [52, 52, 52, 52, 52, 52]

export default {
  draftSixPokemon,
  generateRandomItem,
  draftSixMeowth
}
