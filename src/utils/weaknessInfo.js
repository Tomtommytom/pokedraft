import weaknesses from '@/assets/weaknesses'

const calculateFromTwoTypes = (mainTypeInfo, secondTypeInfo) => {
  const keys = Object.keys(mainTypeInfo)
  let result = {}
  keys.forEach(type => {
    result[type] = mainTypeInfo[type] * secondTypeInfo[type]
  })
  return result
}

export const calculateWeaknesses = (type1, type2) => {
  const mainTypeInfo = weaknesses[type1]
  if (type2) {
    const secondTypeInfo = weaknesses[type2]
    return calculateFromTwoTypes(mainTypeInfo, secondTypeInfo)
  } else return mainTypeInfo
}

export const getKeys = () => {
  return Object.keys(weaknesses)
}
