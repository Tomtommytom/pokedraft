import itemdex from '@/assets/itemdex.json'
export default class Item {
  constructor() {
    const { name, id, desc } = getRandomItem()
    this.name = name
    this.id = id
    this.desc = desc
  }
}

const getRandomItem = () => {
  const items = Object.values(itemdex)
  return items[Math.floor(Math.random() * items.length)]
}
