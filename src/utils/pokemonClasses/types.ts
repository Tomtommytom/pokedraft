export default class Types {
  public main: string
  public secondary: string
  constructor(data: string[]) {
    this.main = data[0]
    this.secondary = data[1]
  }
}
