export interface AbilityData {
  '0': string
  '1'?: string
  H: string
}

export interface TriviaData {
  height: number
  weight: number
  color: string
}

export interface BaseStatData {
  [stat: string]: number
  hp: number
  atk: number
  def: number
  spa: number
  spd: number
  spe: number
}

export interface PokemonData {
  name: string
  id: number
  displayName: string
  types: string[]
  baseStats: BaseStatData
  abilties: AbilityData
  trivia: TriviaData
  sprite: string
  evos?: string[]
  prevos?: string[]
}
