import { AbilityData } from './pokemonInterfaceData'

export default class Abilities {
  public abilities: AbilityData
  constructor(data: AbilityData) {
    this.abilities = data
  }
}
