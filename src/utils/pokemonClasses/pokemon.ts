import Abilities from './abilities'
import Stats from './stats'
import Types from './types'
import { PokemonData, TriviaData } from './pokemonInterfaceData'
import { types } from '@babel/core'

export class Pokemon {
  private price: number
  constructor(
    private name: string,
    private trivia: TriviaData,
    public abilities: Abilities,
    public stats: Stats,
    public types: Types,
    private evos?: string[],
    private prevos?: string[]
  ) {}
}

export function createPokemonFromData(data: PokemonData): Pokemon {
  const abilities = new Abilities(data.abilties)
  const stats = new Stats(data.baseStats)
  const types = new Types(data.types)

  return new Pokemon(
    data.name,
    data.trivia,
    abilities,
    stats,
    types,
    data.evos,
    data.prevos
  )
}
