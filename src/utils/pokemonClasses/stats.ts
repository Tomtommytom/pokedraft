import { BaseStatData } from './pokemonInterfaceData'

export default class Stats {
  constructor(public data: BaseStatData) {}

  baseSum(): number {
    return Object.values(this.data).reduce((sum, stat): number => {
      return sum + stat
    }, 0)
  }
  getOrderedArray(order: string[]): number[] {
    return order.map(stat => this.data[stat])
  }
}
