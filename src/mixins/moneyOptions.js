import { store } from '../store/index'
import { mapState } from 'vuex'

export const moneyOptions = {
  computed: mapState(['money']),
  methods: {
    canAfford(price) {
      return this.money >= price
    },
    subtractMoney(price) {
      store.commit('subtract', price)
    },
    addMoney(value) {
      store.commit('add', value)
    }
  }
}
