import Vuex from 'vuex'
import Vue from 'vue'

Vue.use(Vuex)

export const store = new Vuex.Store({
  state: {
    money: 720
  },
  mutations: {
    add(state, value) {
      state.money += value
    },
    subtract(state, value) {
      state.money -= value
    }
  }
})
